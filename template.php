<?php

/**
 * @file
 * Functions and overrides for the MusoHead theme.
 */

/**
 * Implements template_preprocess_html.
 *
 * @param array $variables
 *   An array of variables to pass to the theme template.
 */
function musohead_preprocess_node(&$variables) {
  $node = $variables['node'];
  $variables['date'] = format_date($node->created, 'custom', 'l, d/m/Y');

  if (variable_get('node_submitted_' . $node->type, TRUE)) {
    $variables['display_submitted'] = TRUE;
    $variables['submitted'] = t('Submitted by !username on !datetime', array('!username' => $variables['name'], '!datetime' => $variables['date']));
    $variables['user_picture'] = theme_get_setting('toggle_node_user_picture') ? theme('user_picture', array('account' => $node)) : '';
  }
  else {
    $variables['display_submitted'] = FALSE;
    $variables['submitted'] = '';
    $variables['user_picture'] = '';
  }
}

/**
 * Implements template_preprocess_html.
 *
 * @param array $variables
 *   An array of variables to pass to the theme template.
 */
function musohead_preprocess_comment(&$variables) {
  $comment = $variables['elements']['#comment'];
  $node = $variables['elements']['#node'];
  $variables['created']   = format_date($comment->created, 'custom', 'l, d/m/Y');
  $variables['changed']   = format_date($comment->changed, 'custom', 'l, d/m/Y');

  $variables['submitted'] = t('Submitted by !username on !datetime', array('!username' => $variables['author'], '!datetime' => $variables['created']));
}
