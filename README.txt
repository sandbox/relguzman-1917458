MusoHead-Theme
=====================

A drupal theme with a responsive, mobile-first design.


ABOUT
=====================

by: Rel Guzman Apaza
r.guzmanap@gmail.com

Personal website: http://muso-head.uni.me/

twitter profile: https://twitter.com/rgapa

github profile: https://github.com/rgap

drupal.org nickname: rgap

drupal blog: http://drupalcamper.blogspot.com/


DESCRIPTION
=====================

- This project comprises a very simple and easy to modify theme.
- The theme has the default settings.
- It has an left Off canvas menu
- Uses HTML5, CSS3
- Responsive design

It is designed to work better with these contrib modules:

http://drupal.org/project/module_filter
http://drupal.org/project/admin_menu

http://drupal.org/project/ctools
http://drupal.org/project/token
http://drupal.org/project/pathauto

http://drupal.org/project/media
http://drupal.org/project/media_youtube
http://drupal.org/project/imce

http://drupal.org/project/views

http://drupal.org/project/webform
http://drupal.org/project/mollom

http://drupal.org/project/sharethis
http://drupal.org/project/facebook_comments_box
http://drupal.org/project/fblikebutton


EXAMPLE OF USAGE
=====================

My personal website

http://muso-head.uni.me/


BROWSER COMPATIBILITY
=====================
The theme has been tested on following browsers:
IE7+, Firefox, Google Chrome, Opera.


DRUPAL COMPATIBILITY
=====================
This theme is compatible with Drupal 7.x.x
