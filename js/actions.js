var menuClosed = 1;
var htmltag = document.documentElement;

jQuery(document).ready(function() {
  jQuery("#menu-link").click(function() {
    if(menuClosed){
      openMenu();
    }
    else{
      closeMenu();
    }
  });
});


function openMenu()
{
  jQuery(htmltag).addClass('js-openMenu');

  menuClosed = 0;
}

function closeMenu()
{

  jQuery(htmltag).removeClass('js-openMenu');

  menuClosed = 1;
}
