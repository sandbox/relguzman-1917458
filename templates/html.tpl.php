<?php
/**
 * @file
 * Default theme implementation to display html.
 */
?>

<!DOCTYPE html>

<html lang="<?php print $language->language; ?>" 
      dir="<?php print $language->dir; ?>"
      <?php print $rdf_namespaces; ?> > <!--RDF namespace prefixes used in the HTML document.-->

<head profile="<?php print $grddl_profile; ?>"> <!--GRDDL profile allowing agents to extract the RDF data.-->

  <?php print $head; ?> <!-- Markup for the HEAD section (including meta tags, keyword tags-->

  <title><?php print $head_title; ?></title>

  <meta name="viewport" content="width=device-width , maximum-scale=1">

  <?php print $styles; ?>
  <?php print $scripts; ?>

</head>

<body class="<?php print $classes; ?>" <?php print $attributes;?>> <!-- String of classes that can be used to style contextually through CSS-->

  <!--NO SE-->
  <div id="skip-link">
    <a href="#main-content" class="element-invisible element-focusable"><?php print t('Skip to main content'); ?></a>
  </div>

  <?php print $page_top; ?> <!--Menu admin,  initial markup from any modules that have altered the page. This variable should always be output first, before all other dynamic content-->
  <?php print $page; ?> <!--rendered page content.-->
  <?php print $page_bottom; ?> <!-- Final closing markup from any modules that have altered the page. -->

</body>
</html>
