<?php

/**
 * @file
 * Default theme implementation to display page.
 */
?>

<div id="outer-cont-body">

  <div id="cont-body">

    <div id="head-regions">

      <div id="header">
        <div id="div-menu-link">
          <a id="menu-link" href="#"></a>
        </div>

        <?php if ($logo): ?>
          <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
            <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
          </a>
        <?php endif; ?>

        <?php if ($site_name): ?>
          <h1 id="site-name">
            <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
          </h1>
        <?php endif; ?>

        <?php if ($site_slogan): ?>
          <h2 id="site-slogan"><?php print $site_slogan; ?></h2>
        <?php endif; ?>

        <?php print render($page['header']); ?> <!--Header region-->

      </div>

      <nav id="menu-cont">
        <div class="menu-block">
          <?php print render($page['menu']); ?>
        </div>
      </nav>

    </div>

    <div id="wrapper">  

      <div id="content-base">

        <!--
        $title = different from head_title, as this is just the node title most of the time.
        $title_prefix, suffix = usados en modulos
        -->

        <?php print render($title_prefix); ?>
          <?php if ($title): ?><h1><?php print $title; ?></h1><?php endif; ?>
        <?php print render($title_suffix); ?>

        <?php print render($messages); ?> <!--HTML for status and error messages, to be displayed at the top of the page.-->

        <?php if ($tabs): ?> <!--HTML for displaying tabs at the top of the page.-->
          <div class="tabs"><?php print render($tabs); ?></div>
        <?php endif; ?>

        <?php if ($action_links): ?>
          <ul class="action-links"><?php print render($action_links); ?></ul>
        <?php endif; ?>

        <?php print render($page['content']); ?> <!--region content-->

        <?php print $feed_icons; ?>

      </div>

      <?php if ($page['sidebar_first']): ?>    
        <div id="sidebar">
          <?php print render($page['sidebar_first']); ?>
        </div>
      <?php endif; ?>  

    </div>

    <div id="footer">
      <?php if ($page['footer']): ?>    
        <?php print render($page['footer']); ?>
      <?php endif; ?>  
    </div>

  </div>

</div>
